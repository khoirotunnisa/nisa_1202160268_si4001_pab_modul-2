package com.example.travel;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.travel.MainActivity;

public class Splashscreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(Splashscreen.class.getSimpleName(), "Oncreate");

        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(Splashscreen.this, MainActivity.class));
                finish();

            }
        }, 3000);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(Splashscreen.class.getSimpleName(), "Onstart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(Splashscreen.class.getSimpleName(), "OnPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(Splashscreen.class.getSimpleName(), "OnResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(Splashscreen.class.getSimpleName(), "OnStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(Splashscreen.class.getSimpleName(), "OnDestroy");
    }
}
